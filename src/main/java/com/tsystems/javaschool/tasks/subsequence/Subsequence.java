package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {
    public boolean find(List x, List y) {
        try {
        if (y.containsAll(x)) {
            for (Object i : x) {
                int first = y.indexOf(i);
                System.out.println(first);
                if (first == -1) return false;
                else {
                    y = y.subList(first + 1, y.size());
                    System.out.println(y);
                }
            }
            return true;
        }
        return false;
        }
        catch(NullPointerException e) {
            throw new IllegalArgumentException();
        }
    }
}
