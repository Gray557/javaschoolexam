package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.lang.*;

class Calculator {
    private static String operators = "+-*/";
    private static String delimiters = "() " + operators;
    private static boolean isDelimiter(String s) {
        return delimiters.contains(s);
    }
    private static boolean isOperator(String s) {
        return operators.contains(s) || s.equals("u-");
    }

    private static int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    private static boolean isNumber(String s) {
        try {
            Double num = Double.valueOf(s);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    public static List<String> parse(String infix) {
        if (infix == null || infix.isEmpty())
            return null;
        List<String> postfix = new ArrayList<String>();
        Deque<String> stack = new ArrayDeque<String>();
        StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
        String prev = "";
        String curr = "";
        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();
            if (!isNumber(curr) && !isDelimiter(curr))
                return null;
            if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                return null;
            }
            if (isOperator(prev) && isOperator(curr)) {;
                return null;
            }
            if (curr.equals(" ")) continue;
            if (isDelimiter(curr)) {
                if (curr.equals("(")) stack.push(curr);
                else if (curr.equals(")")) {
                    if (stack.isEmpty()) {

                        return null;
                    }
                    try {
                        while (!stack.peek().equals("(")) {
                            if (stack.isEmpty()) {
                                //flag = false;
                                return null;
                            }
                            postfix.add(stack.pop());
                        }
                    }
                    catch (NullPointerException e) {
                        return null;
                    }
                    stack.pop();
                    if (!stack.isEmpty()) {
                        postfix.add(stack.pop());
                    }
                    else return null;
                }
                else {
                    if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev)  && !prev.equals(")")))) {
                        // унарный минус
                        curr = "u-";
                    }
                    else {
                        while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                            postfix.add(stack.pop());
                        }

                    }
                    stack.push(curr);
                }
            }
            else {
                postfix.add(curr);
            }
            prev = curr;
        }
        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) postfix.add(stack.pop());
            else {
                //flag = false;
                return null;
            }
        }
        return postfix;
    }

    public static String evaluate(String input) {
        List<String> postfix = parse(input);
        if (postfix == null || postfix.isEmpty()) {
            return null;
        }
        Deque<Double> stack = new ArrayDeque<Double>();
        for (String x : postfix) {
            if (x.equals("+")) stack.push(stack.pop() + stack.pop());
            else if (x.equals("-")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a - b);
            }
            else if (x.equals("*")) stack.push(stack.pop() * stack.pop());
            else if (x.equals("/")) {
                Double b = stack.pop(), a = stack.pop();
                if (b == 0) {
                    return null;
                }
                else stack.push(a / b);
            }
            else if (x.equals("u-")) stack.push(-stack.pop());
            else stack.push(Double.valueOf(x));
        }
        Double answer = stack.pop();
        answer = (double)Math.round(answer * 10000) / 10000;
        return answer.toString().replaceAll("\\.0+$", "");
    }
}